﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GotPapers
{
  public partial class MainForm : Form
  {
    public MainForm()
    {
      InitializeComponent();
    }

    private void ButtonAddFiles_Click(object sender, EventArgs e)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();

      openFileDialog.Filter = "JPG files (*.jpg)|*.jpg|PDF files (*.pdf)|*.pdf|All files (*.*)|*.*";
      openFileDialog.FilterIndex = 0;
      openFileDialog.Multiselect = true;

      if (openFileDialog.ShowDialog() == DialogResult.OK)
      {
        foreach (var fn in openFileDialog.FileNames)
        {
          ListFilenames.Items.Add(fn);
          TextFormat.Text = System.IO.Path.GetExtension(fn).Substring(1).ToUpper();
        }
        CheckZIP.Checked = ListFilenames.Items.Count > 1;
      }
    }

    private void MainForm_DragDrop(object sender, DragEventArgs e)
    {
      string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
      foreach (var fn in files)
      {
        ListFilenames.Items.Add(fn);
        TextFormat.Text = System.IO.Path.GetExtension(fn).Substring(1).ToUpper();
      }
      CheckZIP.Checked = ListFilenames.Items.Count > 1;
    }

    private void MainForm_DragEnter(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(DataFormats.FileDrop))
        e.Effect = DragDropEffects.Copy;
    }

    private void ButtonRemoveSelected_Click(object sender, EventArgs e)
    {
      ListBox.SelectedObjectCollection selectedItems = ListFilenames.SelectedItems;
      for (int i = selectedItems.Count - 1; i >= 0; i--)
        ListFilenames.Items.Remove(selectedItems[i]);
      CheckZIP.Checked = ListFilenames.Items.Count > 1;
    }

    private void ButtonExport_Click(object sender, EventArgs e)
    {
      if (ListFilenames.Items.Count < 1)
      {
        MessageBox.Show("No files given!","GOT PAPERS?",MessageBoxButtons.OK,MessageBoxIcon.Error);
        return;
      }
      if (TextItemName.Text.Length < 1)
      {
        MessageBox.Show("Item name missing!","GOT PAPERS?",MessageBoxButtons.OK,MessageBoxIcon.Error);
        TextItemName.Focus();
        return;
      }
      if (TextDate.Text.Length < 1)
      {
        MessageBox.Show("Date missing!","GOT PAPERS?",MessageBoxButtons.OK,MessageBoxIcon.Error);
        TextDate.Focus();
        return;
      }

      string name = TextItemName.Text + " " + TextDate.Text;
      if (CheckSwapLetter.Checked)
      {
        string s = TextItemName.Text;
        if (s.IndexOf("from ") != -1)
          s = s.Substring(s.IndexOf("from ") + 5);
        else
          MessageBox.Show("Warning: the Item Name field doesn't contain the word \"from\"!", "GOT PAPERS?", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        name = s + " " + TextDate.Text;
      }
      name = System.Text.RegularExpressions.Regex.Replace(name.ToLower(), @"[^a-zA-Z0-9_\-\.\(\)]+", "_");

      SaveFileDialog saveFileDialog = new SaveFileDialog();

      saveFileDialog.FileName = name;
      saveFileDialog.Filter = "ZIP files (*.zip)|*.zip|All files (*.*)|*.*";

      if (saveFileDialog.ShowDialog() == DialogResult.OK)
      {
        Ionic.Zip.ZipFile zip = CheckZIP.Checked ? new Ionic.Zip.ZipFile() : null;

        var dir = System.IO.Path.GetDirectoryName(saveFileDialog.FileName);
        var diz = System.IO.Path.GetTempFileName();
        System.IO.File.WriteAllLines(diz, new string[]{ GetFileIdDiz() } );

        int counter = 1;
        foreach (string item in ListFilenames.Items)
        {
          string p = ListFilenames.Items.Count > 1 ? "_p" + (counter++) : "";
          string newFilename = name + p + System.IO.Path.GetExtension(item);
          if (zip == null)
          {
            System.IO.File.Copy(item, dir + "\\" + newFilename);
          }
          else 
          {
            Ionic.Zip.ZipEntry zipFile = zip.AddFile(item);
            zipFile.FileName = newFilename;
          }
        }
        if (zip == null)
        {
          System.IO.File.Copy(diz, dir + "\\" + name + ".diz");
        }
        else
        {
          Ionic.Zip.ZipEntry zipDiz = zip.AddFile(diz);
          zipDiz.FileName = name + ".diz";
        }

        if (zip != null)
        {
          zip.Save(saveFileDialog.FileName);
        }
        System.IO.File.Delete(diz);
      }
    }

    private string GetFileIdDiz()
    {
      string s = "";

      s += @"               ___" + "\r\n";
      s += @"   ___   _____/  /   .:  g o t   p a p e r s ?  :." + "\r\n";
      s += @" _/   \_/   \_  __\   ___  ____   ___   ___   ___  __/\" + "\r\n";
      s += @"/  /  /  /  /  /__  _/   \_\_  \_/   \_/_  \_/_  \/ /__\" + "\r\n";
      s += @"~\_  /~\___/~\___/ /  ___/  _  /  ___/  /__/  /__/\__ \" + "\r\n";
      s += @"  \_/              ~\/   ~\___/~\/   ~\___/~\/   ~\___/zS!" + "\r\n";
      s += "\r\n";

      s += "  item name: " + TextItemName.Text + "\r\n";
      s += "    creator: " + TextCreator.Text + "\r\n";
      s += "description: " + TextDescription.Text + "\r\n";
      s += "\r\n";

      s += "   category: " + TextCategory.Text + "\r\n";
      s += "       date: " + TextDate.Text + "\r\n";
      s += "   material: " + TextMaterial.Text + "\r\n";
      s += "       size: " + TextSize.Text + "\r\n";
      s += "  print run: " + TextPrintRun.Text + "\r\n";
      s += "\r\n";

      s += "provided by: " + TextProvidedBy.Text + "\r\n";
      s += "    scanned: " + TextScanned.Text + "\r\n";
      s += "     format: " + TextFormat.Text + "\r\n";
      s += "\r\n";

      s += "      notes: " + TextNotes.Text + "\r\n";
      s += "\r\n";

      s += "   got papers? - preserve the scene's material heritage\r\n";
      s += "              http://gotpapers.untergrund.net\r\n";
      return s;
    }

    private void CheckSwapLetter_CheckedChanged(object sender, EventArgs e)
    {
      var IsSwapLetter = ((CheckBox)sender).Checked;
      //TextDate.Visible = !IsSwapLetter;
      //DateSwapLetter.Visible = IsSwapLetter;
    }

    private void DateSwapLetter_ValueChanged(object sender, EventArgs e)
    {
      if (!CheckSwapLetter.Checked) return;
      //TextDate.Value = ((DateTimePicker)sender).Value.Year;
    }

    private void TextDate_ValueChanged(object sender, EventArgs e)
    {
      if (CheckSwapLetter.Checked) return;
      int year = (int)((NumericUpDown)sender).Value;
      //int.TryParse( , out year );
      //System.DateTime d = new System.DateTime(year, 1, 1);
      //DateSwapLetter.Value = d;
    }
  }
}
