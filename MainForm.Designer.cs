﻿namespace GotPapers
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ListFilenames = new System.Windows.Forms.ListBox();
      this.ButtonAddFiles = new System.Windows.Forms.Button();
      this.ButtonRemoveSelected = new System.Windows.Forms.Button();
      this.TextItemName = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.Info = new System.Windows.Forms.GroupBox();
      this.CheckSwapLetter = new System.Windows.Forms.CheckBox();
      this.label3 = new System.Windows.Forms.Label();
      this.TextDescription = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.TextCreator = new System.Windows.Forms.TextBox();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.label8 = new System.Windows.Forms.Label();
      this.TextPrintRun = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.TextSize = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.TextMaterial = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.TextCategory = new System.Windows.Forms.TextBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.label9 = new System.Windows.Forms.Label();
      this.TextFormat = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.TextScanned = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.TextProvidedBy = new System.Windows.Forms.TextBox();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.TextNotes = new System.Windows.Forms.TextBox();
      this.ButtonExport = new System.Windows.Forms.Button();
      this.CheckZIP = new System.Windows.Forms.CheckBox();
      this.TextDate = new System.Windows.Forms.TextBox();
      this.Info.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // ListFilenames
      // 
      this.ListFilenames.FormattingEnabled = true;
      this.ListFilenames.Location = new System.Drawing.Point(12, 12);
      this.ListFilenames.Name = "ListFilenames";
      this.ListFilenames.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
      this.ListFilenames.Size = new System.Drawing.Size(424, 69);
      this.ListFilenames.TabIndex = 0;
      // 
      // ButtonAddFiles
      // 
      this.ButtonAddFiles.Location = new System.Drawing.Point(442, 12);
      this.ButtonAddFiles.Name = "ButtonAddFiles";
      this.ButtonAddFiles.Size = new System.Drawing.Size(128, 35);
      this.ButtonAddFiles.TabIndex = 1;
      this.ButtonAddFiles.Text = "Add files...";
      this.ButtonAddFiles.UseVisualStyleBackColor = true;
      this.ButtonAddFiles.Click += new System.EventHandler(this.ButtonAddFiles_Click);
      // 
      // ButtonRemoveSelected
      // 
      this.ButtonRemoveSelected.Location = new System.Drawing.Point(442, 53);
      this.ButtonRemoveSelected.Name = "ButtonRemoveSelected";
      this.ButtonRemoveSelected.Size = new System.Drawing.Size(128, 28);
      this.ButtonRemoveSelected.TabIndex = 2;
      this.ButtonRemoveSelected.Text = "Remove selected files";
      this.ButtonRemoveSelected.UseVisualStyleBackColor = true;
      this.ButtonRemoveSelected.Click += new System.EventHandler(this.ButtonRemoveSelected_Click);
      // 
      // TextItemName
      // 
      this.TextItemName.Location = new System.Drawing.Point(92, 19);
      this.TextItemName.Name = "TextItemName";
      this.TextItemName.Size = new System.Drawing.Size(158, 20);
      this.TextItemName.TabIndex = 3;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(27, 22);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(59, 13);
      this.label1.TabIndex = 4;
      this.label1.Text = "Item name:";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // Info
      // 
      this.Info.Controls.Add(this.CheckSwapLetter);
      this.Info.Controls.Add(this.label3);
      this.Info.Controls.Add(this.TextDescription);
      this.Info.Controls.Add(this.label2);
      this.Info.Controls.Add(this.TextCreator);
      this.Info.Controls.Add(this.label1);
      this.Info.Controls.Add(this.TextItemName);
      this.Info.Location = new System.Drawing.Point(14, 94);
      this.Info.Name = "Info";
      this.Info.Size = new System.Drawing.Size(275, 128);
      this.Info.TabIndex = 5;
      this.Info.TabStop = false;
      this.Info.Text = "Info:";
      // 
      // CheckSwapLetter
      // 
      this.CheckSwapLetter.AutoSize = true;
      this.CheckSwapLetter.Location = new System.Drawing.Point(92, 97);
      this.CheckSwapLetter.Name = "CheckSwapLetter";
      this.CheckSwapLetter.Size = new System.Drawing.Size(140, 17);
      this.CheckSwapLetter.TabIndex = 9;
      this.CheckSwapLetter.Text = "The item is a swap letter";
      this.CheckSwapLetter.UseVisualStyleBackColor = true;
      this.CheckSwapLetter.CheckedChanged += new System.EventHandler(this.CheckSwapLetter_CheckedChanged);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(23, 74);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(63, 13);
      this.label3.TabIndex = 8;
      this.label3.Text = "Description:";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TextDescription
      // 
      this.TextDescription.Location = new System.Drawing.Point(92, 71);
      this.TextDescription.Name = "TextDescription";
      this.TextDescription.Size = new System.Drawing.Size(158, 20);
      this.TextDescription.TabIndex = 7;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(42, 48);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(44, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Creator:";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TextCreator
      // 
      this.TextCreator.Location = new System.Drawing.Point(92, 45);
      this.TextCreator.Name = "TextCreator";
      this.TextCreator.Size = new System.Drawing.Size(158, 20);
      this.TextCreator.TabIndex = 5;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.TextDate);
      this.groupBox1.Controls.Add(this.label8);
      this.groupBox1.Controls.Add(this.TextPrintRun);
      this.groupBox1.Controls.Add(this.label7);
      this.groupBox1.Controls.Add(this.TextSize);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.TextMaterial);
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.label6);
      this.groupBox1.Controls.Add(this.TextCategory);
      this.groupBox1.Location = new System.Drawing.Point(14, 228);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(275, 161);
      this.groupBox1.TabIndex = 6;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Author:";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(37, 126);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(49, 13);
      this.label8.TabIndex = 12;
      this.label8.Text = "Print run:";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TextPrintRun
      // 
      this.TextPrintRun.Location = new System.Drawing.Point(92, 123);
      this.TextPrintRun.Name = "TextPrintRun";
      this.TextPrintRun.Size = new System.Drawing.Size(158, 20);
      this.TextPrintRun.TabIndex = 4;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(56, 100);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(30, 13);
      this.label7.TabIndex = 10;
      this.label7.Text = "Size:";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TextSize
      // 
      this.TextSize.Location = new System.Drawing.Point(92, 97);
      this.TextSize.Name = "TextSize";
      this.TextSize.Size = new System.Drawing.Size(158, 20);
      this.TextSize.TabIndex = 3;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(39, 74);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(47, 13);
      this.label4.TabIndex = 8;
      this.label4.Text = "Material:";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TextMaterial
      // 
      this.TextMaterial.Location = new System.Drawing.Point(92, 71);
      this.TextMaterial.Name = "TextMaterial";
      this.TextMaterial.Size = new System.Drawing.Size(158, 20);
      this.TextMaterial.TabIndex = 2;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(53, 48);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(33, 13);
      this.label5.TabIndex = 6;
      this.label5.Text = "Date:";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(34, 22);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(52, 13);
      this.label6.TabIndex = 4;
      this.label6.Text = "Category:";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TextCategory
      // 
      this.TextCategory.Location = new System.Drawing.Point(92, 19);
      this.TextCategory.Name = "TextCategory";
      this.TextCategory.Size = new System.Drawing.Size(158, 20);
      this.TextCategory.TabIndex = 0;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.label9);
      this.groupBox2.Controls.Add(this.TextFormat);
      this.groupBox2.Controls.Add(this.label10);
      this.groupBox2.Controls.Add(this.TextScanned);
      this.groupBox2.Controls.Add(this.label11);
      this.groupBox2.Controls.Add(this.TextProvidedBy);
      this.groupBox2.Location = new System.Drawing.Point(295, 94);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(275, 110);
      this.groupBox2.TabIndex = 7;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Source:";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(44, 74);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(42, 13);
      this.label9.TabIndex = 8;
      this.label9.Text = "Format:";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TextFormat
      // 
      this.TextFormat.Location = new System.Drawing.Point(92, 71);
      this.TextFormat.Name = "TextFormat";
      this.TextFormat.Size = new System.Drawing.Size(158, 20);
      this.TextFormat.TabIndex = 7;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(33, 48);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(53, 13);
      this.label10.TabIndex = 6;
      this.label10.Text = "Scanned:";
      this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TextScanned
      // 
      this.TextScanned.Location = new System.Drawing.Point(92, 45);
      this.TextScanned.Name = "TextScanned";
      this.TextScanned.Size = new System.Drawing.Size(158, 20);
      this.TextScanned.TabIndex = 5;
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(20, 22);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(66, 13);
      this.label11.TabIndex = 4;
      this.label11.Text = "Provided by:";
      this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TextProvidedBy
      // 
      this.TextProvidedBy.Location = new System.Drawing.Point(92, 19);
      this.TextProvidedBy.Name = "TextProvidedBy";
      this.TextProvidedBy.Size = new System.Drawing.Size(158, 20);
      this.TextProvidedBy.TabIndex = 3;
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.TextNotes);
      this.groupBox3.Location = new System.Drawing.Point(295, 210);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(275, 179);
      this.groupBox3.TabIndex = 8;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Notes:";
      // 
      // TextNotes
      // 
      this.TextNotes.Location = new System.Drawing.Point(23, 19);
      this.TextNotes.Multiline = true;
      this.TextNotes.Name = "TextNotes";
      this.TextNotes.Size = new System.Drawing.Size(227, 142);
      this.TextNotes.TabIndex = 0;
      // 
      // ButtonExport
      // 
      this.ButtonExport.Location = new System.Drawing.Point(187, 398);
      this.ButtonExport.Name = "ButtonExport";
      this.ButtonExport.Size = new System.Drawing.Size(208, 45);
      this.ButtonExport.TabIndex = 9;
      this.ButtonExport.Text = "Export";
      this.ButtonExport.UseVisualStyleBackColor = true;
      this.ButtonExport.Click += new System.EventHandler(this.ButtonExport_Click);
      // 
      // CheckZIP
      // 
      this.CheckZIP.AutoSize = true;
      this.CheckZIP.Location = new System.Drawing.Point(401, 413);
      this.CheckZIP.Name = "CheckZIP";
      this.CheckZIP.Size = new System.Drawing.Size(93, 17);
      this.CheckZIP.TabIndex = 10;
      this.CheckZIP.Text = "Create ZIP file";
      this.CheckZIP.UseVisualStyleBackColor = true;
      // 
      // TextDate
      // 
      this.TextDate.Location = new System.Drawing.Point(92, 45);
      this.TextDate.Name = "TextDate";
      this.TextDate.Size = new System.Drawing.Size(158, 20);
      this.TextDate.TabIndex = 1;
      // 
      // MainForm
      // 
      this.AllowDrop = true;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(582, 455);
      this.Controls.Add(this.CheckZIP);
      this.Controls.Add(this.ButtonExport);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.Info);
      this.Controls.Add(this.ButtonRemoveSelected);
      this.Controls.Add(this.ButtonAddFiles);
      this.Controls.Add(this.ListFilenames);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "MainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "GOT PAPERS?";
      this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
      this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
      this.Info.ResumeLayout(false);
      this.Info.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ListBox ListFilenames;
    private System.Windows.Forms.Button ButtonAddFiles;
    private System.Windows.Forms.Button ButtonRemoveSelected;
    private System.Windows.Forms.TextBox TextItemName;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox Info;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox TextDescription;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox TextCreator;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox TextPrintRun;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox TextSize;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox TextMaterial;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox TextCategory;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox TextFormat;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox TextScanned;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox TextProvidedBy;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.TextBox TextNotes;
    private System.Windows.Forms.Button ButtonExport;
    private System.Windows.Forms.CheckBox CheckZIP;
    private System.Windows.Forms.CheckBox CheckSwapLetter;
    private System.Windows.Forms.TextBox TextDate;

  }
}

